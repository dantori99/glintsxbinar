// functionArgumen
function balokArg (panjang, lebar, tinggi) {
    console.log("Hasil mBalok =>" + " " + (panjang * lebar * tinggi));
}

function kubusArg (sisi) {
    console.log("Hasil mKubus =>" + " " + (sisi ** 3));
}

balokArg(2, 3, 4);
kubusArg(2);

// functionReturn
function balok(panjang, lebar, tinggi) {
    let balok = panjang * lebar * tinggi;
    return balok;
}
function kubus(sisi) {
    let kubus = sisi ** 3;
    return kubus;
}
function jumlahGeo(panjang, lebar, tinggi, sisi) {
    let jumlahGeo = balok(panjang, lebar, tinggi) + kubus(sisi);
    return jumlahGeo;
}

// operasiMatematika
let volumeBalok = balok(2, 3, 4);
console.log(`Berikut Volume Balok = ${volumeBalok}`);

let volumeKubus = kubus(2);
console.log(`Berikut Volume Kubus = ${volumeKubus}`);

let hasil = jumlahGeo(2, 3, 4, 2);
console.log(`Berikut Hasil Penjumlahan Kedua Bidang Ruang = ${hasil}`);
