const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function volume(phi, jariJari, tinggi) {
  return (phi * (jariJari ** 2)) * tinggi;
}

function ijariJari() {
  rl.question(`jari-jari: `, (jariJari) => {
    if (!isNaN(jariJari)) {
      iVolume(jariJari);
    } else {
      console.log(`jari2 harus berupa angka\n`);
      ijariJari();
    }
  });
}

function iVolume(jariJari) {
  rl.question(`tinggi: `, (tinggi) => {
    if (!isNaN(tinggi)) {
      const phi = 3.14;
      console.log(`volume tabung = ${volume(phi, jariJari, tinggi)} cm`);
      rl.close();
    } else {
      console.log(`tinggi harus berupa angka\n`);
      iVolume(jariJari);
    }
  });
}

console.log(`Volume Tabung B)`);
console.log(`=========`);
ijariJari(); 