const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function volume(phi, jariJari, tinggi) {
    return (phi * (jariJari ** 2)) * tinggi;
}
function input() {
  rl.question('Jari-jari: ', function (jariJari) {
    rl.question('Tinggi: ', (tinggi) => {
        if (jariJari > 0 && tinggi > 0) {
          const phi = 3.14;
          console.log(`\nVolume Tabung= ${volume(phi, jariJari, tinggi)} cm`);
          rl.close();
        } else {
          console.log(`jari-jari dan tinggi harus berupa angka\n`);
          input();
        }
    });
  });
}

console.log(`Volume Tabung 2.0`);
console.log(`=========`);
input(); 
