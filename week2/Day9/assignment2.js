const people = [
    {
        "name": "John",
        "status": "Positive"
    },
    {
        "name": "Mike",
        "status": "Suspect"
    },
    {
        "name": "Elon",
        "status": "Negative"
    },
    {
        "name": "Musk",
        "status": "Suspect"
    },
    {
        "name": "Maye",
        "status": "Positive"
    },
    {
        "name": "Daniel",
        "status": "Suspect"
    },
    {
        "name": "Harianja",
        "status": "Negative"
    },
    {
        "name": "Fiend",
        "status": "Positive"
    },
    {
        "name": "Slardar",
        "status": "Negative"
    }
]
// function pasien(hasilTest) {
//     for (let i = 0; i < people.length; i++){
//         switch (people[i].status) {
//             case hasilTest:
//                 console.log(`${hasilTest} : ${people[i].name}`);
//                 break      
//             default:
//                 break
//         }
//     }
// }

// pasien("Positive");
// pasien("Negative");
// pasien("Suspect");

// function pasien(hasilTest){
//     let i = 0;
//     while (i < people.length) {
//         if (people[i].status == hasilTest) {
//             console.log(`${hasilTest} : ${people[i].name}`);
//         }
//         i++;
//     }    
// }
// pasien("Positive");
// pasien("Negative");
// pasien("Suspect");

// function pasien(hasilTest) {
//     let a = [];
//     for (let i = 0; i < people.length; i++) {
//         if (people[i].status == hasilTest) {
//             a.push(people[i].name);
//             // console.log(`berikut ${hasilTest} : ${people[i].name}`);
//         }
//     }
//     return a
// }

function pasien(hasilTest) {
    let a = [];
    let i = 0;
    console.log(`${hasilTest} :`);
    do {        
        if (people[i].status == hasilTest) {
            a.push(people[i].name);
            // console.log(`${hasilTest} : ${people[i].name}`);
        }
        i++;
    } while(i < people.length);
    return a;
}

// console.log(pasien("Positive"));
// console.log(pasien("Negative"));
// console.log(pasien("Suspect"));

module.exports = pasien;