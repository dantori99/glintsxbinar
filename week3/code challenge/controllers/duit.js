let data = require('../models/kripto.json');

// ======
class Duit {
    ambilDuit(req, res, next) {
        try {
            res.status(200).json({ daftarDuit: data });
        } catch (error) {
            res.status(500).json({
                mengError: ['Sabar ngab'],
            });
        }
    }
//  ======
    detailDuit(req, res, next) {
        try {
            let detailDuit = data.filter(
                (item) => item.id === parseInt(req.params.id)
            );

            if (detailDuit.length === 0) {
                return res.status(404).json({ error: ['Tidak ditemukan'] });
            }
            res.status(200).json({ detail: detailDuit });
        } catch (error) {
            res.status(500).json({
                error: ['Sabar ngab'],
            })
        }
    } 
// ======
    nambahDuit(req, res, next) {
        try {
            let lastId = data[data.length - 1].id;

            data.push({
                id: lastId + 1,
                nama: req.body.nama,
                nick: req.body.nick,
                ath: req.body.ath,
                atl: req.body.atl
            });

            res.status(201).json({ nambah: data });
        } catch (error) {
            res.status(500).json({
                error: ['Sabar ngab'],
            });
        }
    }
// ======
    updateDuit(req, res, next) {
        try {
            let findDuit = data.some((item) => item.id === parseInt(req.params.id));
        
            // kalo ga nemu
            if(!findDuit) {
                return res.status(404).json({ error: ['Tidak ditemukan'] });
            }

            data = data.map((item) => 
            item.id === parseInt(req.params.id)
            ? {
                id: parseInt(req.params.id),
                nama: req.body.nama,
                nick: req.body.nick,
                ath: req.body.ath,
                atl: req.body.atl
            }
                : item
            );
            res.status(200).json({ data: data });
        } catch (error) {   
            res.status(500).json({
                error: ['Sabar ngab'],
            });
        }
    }
// ======
    hapusDuit(req, res, next) {
        try {
            let findDuit = data.some((item) => item.id === parseInt(req.params.id));
            if(!findDuit) {
                return res.status(404).json({ error: ['Tidak ditemukan'] });
            }

            data = data.filter((item) => item.id !== parseInt(req.params.id));

            res.status(200).json({ data: data });
        } catch (error) {
            res.status(500).jeson({
                error: ['Sabar ngab'],
            })
        }
    }
}
// ======
module.exports = new Duit();
