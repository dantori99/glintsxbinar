// impor module express nya
const express = require('express');
// bikin app express nya
const app = express();
// impor rute
const duit = require("./routes/duit")

// port nya
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// handle client when client access to http://localhost:3000/students
app.use('/duit', duit);

// run this application on port 3000
app.listen(port, () => {
    console.log(`Server running on ${port}!`);
});