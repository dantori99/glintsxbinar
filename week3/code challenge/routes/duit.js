const express = require('express');

const {
    ambilDuit,
    detailDuit,
    nambahDuit,
    updateDuit,
    hapusDuit
} = require('../controllers/duit');

// bikin router
const router = express.Router();

// jika client req ke https://localhost:3000/duit function ambilDuit called
router.get('/', ambilDuit);

// jika client req ke https://localhost:3000/duit/:id function detailDuit called
router.get('/:id', detailDuit);

// untuk nambah(POST) duit baru
router.post('/', nambahDuit);

// untuk update(PUT) duit 
router.put('/:id', updateDuit);

// untuk hapus(DELETE) duit
router.delete('/:id', hapusDuit);

module.exports = router;