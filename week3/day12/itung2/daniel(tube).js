const ThreeDimention = require('./threedimention');

class Tube extends ThreeDimention {
  constructor(radius, height) {
    super('Sir Daniel');
    this.radius = radius;
    this.height = height;
  }
  
  calculateArea() {
    super.calculateArea();
    const phi = 3.14
    let luasPermukaan = 2 * phi * this.radius * this.height;
    return luasPermukaan
  }

  calculateVolume() {
    super.calculateVolume();
    const phi = 3.14
    let volumeTube = (this.radius ** 2) * phi * this.height;
    return volumeTube
  }
}

module.exports = Tube;
