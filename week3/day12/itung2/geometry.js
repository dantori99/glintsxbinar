class Geometry {
  constructor(name, type) {
    this.name = name;
    this.type = type;
  }

  hello() {
    console.log(`${this.name}, Hello!`);
  }
}

module.exports = Geometry;
