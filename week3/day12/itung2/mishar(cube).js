const ThreeDimention = require('./threedimention');

class Cube extends ThreeDimention {
  constructor(sisi) {
    super('Cube');
    this.sisi = sisi;
    
  }
  // Overriding
  calculateArea() {
    super.calculateArea(); 
    let Area = ((this.sisi ** 2) * 6);
    return Area;
  }

  calculateVolume() {
    super.calculateVolume();
    let volume = (this.sisi ** 3)
    return volume
  }

}

module.exports = Cube ;
