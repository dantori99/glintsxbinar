const Geometry = require('./geometry');

class ThreeDimention extends Geometry {
  constructor(name) {
    super(name, 'Three Dimention');
    // new Geometry('Tube', 'Three Dimention')

    // if (this.constructor === TwoDimention) {
    //   throw new Error('Cannot instantiate from Abstract Class'); // Because it's abstract
    // }
  }

  calculateArea() {
    console.log(`${this.name} is calculating area!`);
  }

  calculateVolume() {
    console.log(`${this.name} is calculating area!`);
  }
}

module.exports = ThreeDimention;
