const { Tube, Beam, Cube } = require('./itung2/index');

// Create Object
let tubeOne = new Tube(5, 5);
let beamOne = new Beam(5, 5, 5);
let cubeOne = new Cube(5);

// Calculate Area
let a = tubeOne.calculateArea();
let b = beamOne.calculateArea();
let c = cubeOne.calculateArea();
let d = a + b + c;
console.log(d);

// Calculate Circumference
let e = tubeOne.calculateVolume();
let f = beamOne.calculateVolume();
let g = cubeOne.calculateVolume();
let h = e + f + g;
console.log(h);
