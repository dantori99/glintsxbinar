const axios = require("axios");

let urlA = "https://www.boredapi.com/api/activity";
let urlB = "https://api.genderize.io/?name=luc";
let urlC = "https://mocki.io/v1/d4867d8b-b5d5-4a48-a4ab-79131b5809b8";
let data = {};

const fetchApi = async () => {
  try {
    let response = await Promise.all([
      axios.get(urlA),
      axios.get(urlB),
      axios.get(urlC),
    ]);

    data = {
      activity: response[0].data,
      gender: response[1].data,
      nationality: response[2].data,
    };

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

fetchApi();
