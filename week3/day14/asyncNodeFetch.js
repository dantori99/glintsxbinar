const fetch = require('node-fetch');
const urlact = 'https://www.boredapi.com/api/activity' 
const urlgen = 'https://api.genderize.io/?name=luc'
const urlnat = 'https://mocki.io/v1/d4867d8b-b5d5-4a48-a4ab-79131b5809b8'
let tampung = {

};

async function fetchApi() {
  let response = await Promise.all([
    fetch(urlact),
    fetch(urlgen),
    fetch(urlnat),
  ]);
  tampung = {
    activity: await response[0].json(),
    gender: await response[1].json(),
    nationality: await response[2].json(),
  };
  console.log(tampung);
};
fetchApi();
// console.log(data);