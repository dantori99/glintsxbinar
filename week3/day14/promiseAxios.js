const axios = require('axios');

let urlA = 'https://www.boredapi.com/api/activity';
let urlB = 'https://api.genderize.io/?name=luc';
let urlC = 'https://mocki.io/v1/d4867d8b-b5d5-4a48-a4ab-79131b5809b8';
let data = {};

axios.get(urlA)
  .then((response) => {
    data = { activity: response.data };
    return axios.get(urlB);
  })
  .then((response) => {
    data = { ...data, gender: response.data };
    return axios.get(urlC);
  })
  .then((response) => {
    data = { ...data, typeOfNationality: response.data };
    console.log(data);
  })
  .catch((err) => {
    console.error(err.message);
  });
