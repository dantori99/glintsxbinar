const fetch = require('node-fetch');

const urlact = 'https://www.boredapi.com/api/activity' 
const urlgen = 'https://api.genderize.io/?name=luc'
const urlnat = 'https://mocki.io/v1/d4867d8b-b5d5-4a48-a4ab-79131b5809b8'
let tampung = {

};

fetch(urlact)
  .then((response) => {
    tampung = { activity: response.json() };
    return fetch(urlgen);
  })
  .then((response) => {
    tampung = { ...tampung, gender: response.json() };
    return fetch(urlnat);
  })
  .then((response) => {
    tampung = { ...tampung, nationality: response.json() };
    console.log(tampung);
  })
  .catch((err) => {
    console.error(err.message);
  });
