const { customer } = require("../models");

class Customer {
  async getAllCustomer(req, res, next) {
    try {
      const data = await customer.findAll({
        attributes: {},
      });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["Customer not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  async getDetailCustomer(req, res, next) {
    try {
      let data = await customer.findOne({
        // find all data of Customer table
        where: { id: req.params.id },
      });

      if (!data) {
        return res.status(404).json({ errors: ["Customer not found"] });
      }

      res.status(200).json({ data });
    } catch (e) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async createCustomer(req, res, next) {
    try {
      const newData = await customer.create(req.body);

      const data = await customer.findOne({
        where: {
          id: newData.id,
        },
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateCustomer(req, res, next) {
    try {
      const updatedData = await customer.update(req.body, {
        where: {
          id: req.params.id,
        },
      });

      if (updatedData[0] === 0) {
        return res.status(404).json({ errors: ["Customer not found"] });
      }

      const data = await customer.findOne({
        where: {
          id: req.params.id,
        },
      });

      res.status(201).json({ data });
    } catch (e) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteCustomer(req, res, next) {
    try {
      let data = await customer.destroy({ where: { id: req.params.id } });

      if (!data) {
        return res.status(404).json({ errors: ["Customer not found"] });
      }

      res.status(200).json({ message: "Success delete Customer" });
    } catch (e) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Customer();
