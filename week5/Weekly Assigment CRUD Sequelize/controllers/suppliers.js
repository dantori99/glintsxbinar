const { supplier } = require("../models");

class Supplier {
  async getAllSupplier(req, res, next) {
    try {
      // Create Supplier
      // const newData = await supplier.create(req.body);

      // Find Supplier
      const data = await supplier.findAll({
        // where: {
        //   id: newData.id,
        // },
        attributes: {},
      });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["Supplier not found"] });
      }

      // If success
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  async getDetailSupplier(req, res, next) {
    try {
      let data = await supplier.findOne({
        // find all data of Supplier table
        where: { id: req.params.id },
      });

      // If data is nothing
      if (!data) {
        return res.status(404).json({ errors: ["Supplier not found"] });
      }

      // If success
      res.status(200).json({ data });
    } catch (e) {
      // If error
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async createSupplier(req, res, next) {
    try {
      const newData = await supplier.create(req.body);

      const data = await supplier.findOne({
        where: {
          id: newData.id,
        },
        attributes: {},
      });

      res.status(201).json({ data });
    } catch (error) {
      console.error(typeof supplier);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateSupplier(req, res, next) {
    try {
      const updatedData = await supplier.update(req.body, {
        where: {
          id: req.params.id,
        },
      });

      if (updatedData[0] === 0) {
        return res.status(404).json({ errors: ["Supplier not found"] });
      }

      const data = await supplier.findOne({
        where: {
          id: req.params.id,
        },
      });

      res.status(201).json({ data });
    } catch (e) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  async deleteSupplier(req, res, next) {
    try {
      let data = await supplier.destroy({ where: { id: req.params.id } });

      if (!data) {
        return res.status(404).json({ errors: ["Supplier not found"] });
      }

      res.status(200).json({ message: "Success delete Supplier" });
    } catch (e) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Supplier();
