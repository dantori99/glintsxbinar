const express = require("express"); // Import express
const fileUpload = require("express-fileupload");

const suppliers = require("./routes/suppliers");
const customers = require("./routes/customers");
const goods = require("./routes/goods");
const transactions = require("./routes/transactions");

const port = process.env.PORT || 3000; // Define port

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(fileUpload());
app.use(express.static("public"));

app.use("/suppliers", suppliers);
app.use("/customers", customers);
app.use("/goods", goods);
app.use("/transactions", transactions);

app.listen(port, () => console.log(`Server running on port ${port}`));
