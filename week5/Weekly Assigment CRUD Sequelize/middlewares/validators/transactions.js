const path = require("path");
const crypto = require("crypto");
const validator = require("validator");
const { promisify } = require("util");
const { good } = require("../../models");
exports.createTransactionValidator = async (req, res, next) => {
  try {
    const errors = [];

    if (!validator.isInt(req.body.quantity)) {
      errors.push("quantity must be number");
    }

    if (!validator.isInt(req.body.id_good)) {
      errors.push("id_good must be number");
    }

    if (!validator.isInt(req.body.id_customer)) {
      errors.push("id_customer must be number");
    }

    if (errors.length > 0) {
      return res.status(400).json({ errors: errors });
    }
    const findGood = await good.findOne({ where: { id: req.body.id_good } });
    if (!findGood) {
      return res.status(500).json({ error: ["gaada good"] });
    }
    const { price } = findGood;
    req.body.total = parseFloat(price) * parseFloat(req.body.quantity);

    next();
  } catch (error) {
    console.log(error);
    res.status(500).json({ errors: ["Internal Server Error"] });
  }
};
