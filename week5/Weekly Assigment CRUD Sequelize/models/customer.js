"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Customer extends Model {
    static associate(models) {
      models.customer.hasMany(models.transaction, {
        foreignKey: "id_customer",
      });
    }
  }
  Customer.init(
    {
      name: DataTypes.STRING,
      image: DataTypes.STRING,
    },
    {
      sequelize,
      paranoid: true,
      timestamps: true,
      modelName: "customer",
    }
  );
  return Customer;
};
