"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Good extends Model {
    static associate(models) {
      models.good.belongsTo(models.supplier, {
        foreignKey: "id_supplier",
      });
      models.good.hasMany(models.transaction, {
        foreignKey: "id_good",
      });
    }
  }
  Good.init(
    {
      name: DataTypes.STRING,
      price: DataTypes.DECIMAL,
      id_supplier: DataTypes.INTEGER,
      image: DataTypes.STRING,
    },
    {
      sequelize,
      paranoid: true,
      timestamps: true,
      modelName: "good",
    }
  );
  return Good;
};
