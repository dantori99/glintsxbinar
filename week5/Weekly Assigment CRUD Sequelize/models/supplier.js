"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Supplier extends Model {
    static associate(models) {
      models.supplier.hasMany(models.good, {
        foreignKey: "id_supplier",
      });
    }
  }
  Supplier.init(
    {
      name: DataTypes.STRING,
      image: DataTypes.STRING,
    },
    {
      sequelize,
      paranoid: true,
      timestamps: true,
      modelName: "supplier",
    }
  );
  return Supplier;
};
