"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Transaction extends Model {
    static associate(models) {
      models.transaction.belongsTo(models.customer, {
        foreignKey: "id_customer",
      });
      models.transaction.belongsTo(models.good, {
        foreignKey: "id_good",
      });
    }
  }
  Transaction.init(
    {
      id_good: DataTypes.INTEGER,
      id_customer: DataTypes.INTEGER,
      quantity: DataTypes.INTEGER,
      total: DataTypes.DECIMAL,
    },
    {
      sequelize,
      paranoid: true, // Enable soft delete
      timestamps: true, // Enable createdAt and updatedAt
      modelName: "transaction",
    }
  );
  return Transaction;
};
