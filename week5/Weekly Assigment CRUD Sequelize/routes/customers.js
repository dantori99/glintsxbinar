const express = require("express");

const {
  createCustomerValidator,
} = require("../middlewares/validators/customers");

const {
  getAllCustomer,
  createCustomer,
  getDetailCustomer,
  deleteCustomer,
  updateCustomer,
} = require("../controllers/customers");

const router = express.Router();

router
  .route("/")
  .get(getAllCustomer)
  .post(createCustomerValidator, createCustomer);

router
  .route("/:id")
  .get(getDetailCustomer)
  .put(createCustomerValidator, updateCustomer)
  .delete(deleteCustomer);

module.exports = router;
