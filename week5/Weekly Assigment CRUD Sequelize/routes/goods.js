const express = require("express");

const { createGoodValidator } = require("../middlewares/validators/goods");

const {
  getAllGood,
  createGood,
  getDetailGood,
  deleteGood,
  updateGood,
} = require("../controllers/goods");

const router = express.Router();

router.route("/").get(getAllGood).post(createGoodValidator, createGood);

router
  .route("/:id")
  .get(getDetailGood)
  .put(createGoodValidator, updateGood)
  .delete(deleteGood);

module.exports = router;
