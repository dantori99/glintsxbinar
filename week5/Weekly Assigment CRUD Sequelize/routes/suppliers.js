const express = require("express");

const {
  createSupplierValidator,
} = require("../middlewares/validators/suppliers");

const {
  getAllSupplier,
  createSupplier,
  getDetailSupplier,
  deleteSupplier,
  updateSupplier,
} = require("../controllers/suppliers");

const router = express.Router();

router
  .route("/")
  .get(getAllSupplier)
  .post(createSupplierValidator, createSupplier);

router
  .route("/:id")
  .get(getDetailSupplier)
  .put(createSupplierValidator, updateSupplier)
  .delete(deleteSupplier);

module.exports = router;
