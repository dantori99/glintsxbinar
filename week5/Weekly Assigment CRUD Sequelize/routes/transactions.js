const express = require("express");

const {
  createTransactionValidator,
} = require("../middlewares/validators/transactions");

const {
  getAllTransactions,
  createTransaction,
  getDetailTransaction,
  deleteTransaction,
  updateTransaction,
} = require("../controllers/transactions");

const router = express.Router();

router
  .route("/")
  .get(getAllTransactions)
  .post(createTransactionValidator, createTransaction);

router
  .route("/:id")
  .get(getDetailTransaction)
  .put(createTransactionValidator, updateTransaction)
  .delete(deleteTransaction);

module.exports = router;
