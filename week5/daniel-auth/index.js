const express = require("express");

const user = require("./routes/user");
const tugas = require("./routes/task");

const errorHandler = require("./middlewares/errorHandler");

const port = process.env.PORT || 3000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/user", user);
app.use("/tugas", tugas);

app.all("*", (req, res, next) => {
  next({ statusCode: 404, message: "Endpoint not found" });
});

app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server running on ${port}`);
});
